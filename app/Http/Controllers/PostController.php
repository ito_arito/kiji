<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Post;
use Auth;
use Validator;

class PostController extends Controller
{
    /**
     * 記事アプリはログインして使われることを想定しているためauth認証を行う
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 記事一覧ページを表示する
     *
     * @return Response
     */
    public function index() {
        $posts = Post::orderBy('created_at', 'desc')->paginate(10);
        return view('post.index', ['posts' => $posts]);
    }

    /**
     * 記事エディタページを表示する
     * すでに登録されている場合、入力されている状態で表示する
     *
     * @param integer $post_id
     * @return Response
     */
    public function edit(int $post_id = null) {
        if ($post_id === null) {
            $post = new Post;
        } else {
            $post = Auth::user()->posts()->find($post_id) ?? new Post();
        }
        return view('post.edit', ['post' => $post]);
    }

    /**
     * 記事を登録・更新するメソッド
     * すでに登録されている記事であれば入力で上書きし、新規作成であればレコードを作成する
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'post.title' => 'required|max:255',
            'post.content' => 'required|max:8191'
        ]);
    
        $input_post = $request->input('post');
        $input_post['user_id'] = Auth::user()->id;

        $post = Auth::user()->posts()->find($request->post_id);

        if ($post === null) {
            Post::create($input_post);
        } else {
            $post->fill($input_post);
            $post->save();
        }
    
        return redirect('/');
    }

    /**
     * 記事の詳細を表示する
     *
     * @param Post $post
     * @return Response
     */
    public function show(Post $post)
    {
        return view('post.detail', [
            'post' => $post,
        ]);
    }

    /**
     * 記事の削除を行う
     *
     * @param integer $post_id
     * @return Response
     */
    public function delete(int $post_id)
    {
        $post = Auth::user()->posts()->find($post_id);
        $post and $post->delete();
        return redirect('/');
    }
}
