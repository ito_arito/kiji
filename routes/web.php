<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'PostController@index');

Route::get('/edit/{post_id?}', 'PostController@edit');

Route::post('/post', 'PostController@store');

Route::get('/post/{post}', 'PostController@show');

Route::delete('/post/{post_id}', 'PostController@delete');

Route::get('/home', 'PostController@index');
Route::get('/', 'PostController@index');
