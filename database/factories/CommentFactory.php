<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(App\User::class)->create()->id;
        },
        'title' => $faker->word(),
        'content' => $faker->word(),
        'published_at' => $faker->dateTime('now')
    ];
});