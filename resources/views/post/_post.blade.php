<div class="row py-3">
	<div class="col-lg-2 col-12 mb-1">
		{{ $post->created_at }}
	</div>
	<div class="col-lg-8 col-12 title mb-1">
		<a href="{{ url('post/' . $post->id) }}">
			<h4>{{ $post->title }}</h4>
		</a>
	</div>
	<div class="col-auto col-lg-1 mb-1">
		<div class="edit">
			<a href="{{ url('edit/' . $post->id) }}">
				<button class="btn btn-primary">編集</button>
			</a>
		</div>
	</div>
	<div class="col-auto col-lg-1 mb-1">
		<form action="{{ url('post/' . $post->id) }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('DELETE') }}

			<button type="submit" class="btn btn-danger">
				<i class="far fa-trash-alt"></i>
			</button>
		</form>
	</div>
</div>