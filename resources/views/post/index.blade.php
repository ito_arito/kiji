@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div id="timeline" class="mb-4">
                <h4 class="text-center mb-4">T I M E L I N E</h4>
                @each('post._post', $posts, 'post')
            </div>

            @unless (empty($posts))
            {{ $posts->links() }}
            @endunless
        </div>
    </div>
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/post/index.css') }}">
@endsection
