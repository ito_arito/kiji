@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div id="article">
                <div class="card bg-white">

                    <div class="card-header">
                        <div class="clearfix mb-2">
                            <div class="float-left">
                                <span class="item-date ml-3 text-muted">{{ $post->updated_at }}に更新</span>
                            </div>
                            <div class="float-right">
                                <button type="button" class="mr-2 btn btn-outline-secondary btn-unlike">
                                    <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                                    <span class="like-cnt"></span>
                                </button>
                                <button type="button" class="btn btn-outline-secondary btn-like">
                                    <span class="fa fa-thumbs-down" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                        <h1 class="card-title mb-3">{{ $post->title }}</h1>
                    </div>

                    <div class="card-body">
                        <p class="card-text">{{ $post->content }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/post/detail.css') }}">
@endsection