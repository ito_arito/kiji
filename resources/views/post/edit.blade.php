@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="form-editor">
                <form action="{{ url('post') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ $post->id }}" name="post_id">
                    <div class="form-group">
                        <label for="inputTitle">タイトル</label>
                        <input type="text" name="post[title]" class="form-control" id="inputTitle" maxlength="255"
                                placeholder="タイトル" value="{{ $post->title ?? old('title') }}">
                    </div>
                    <div class="form-group">
                        <label for="inputTags">タグ</label>
                        <input type="text" name="tags" class="form-control" id="inputTags"
                                maxlength="255" value="{{ old('tagas') }}" placeholder="スペース区切りでタグを入力">
                    </div>
                    <div class="form-group">
                        <label for="textareaContent">内容</label>
                        <textarea name="post[content]" class="form-control" id="textareaContent" rows="15" maxlength="255"
                                    placeholder="Markdownでオナシャス">{{ $post->content ?? old('content') }}</textarea>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" value="send">投稿する</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
